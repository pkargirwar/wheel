$(document).ready(start);

function start()
{
	$("#test-div").css({opacity: 0.2}).wheel({
		color: 'black',
		background: 'white',
		width: 8,
		height: 20,
		spokes: 19,
		fallback: 'Please wait'});

	$("#test2-div").css({opacity: 0.2}).wheel({
		color: 'blue',
		background: 'rgba(255, 255, 255, 0)',
		width: 6,
		height: 12,
		spokes: 15,
		position: 'left'});

	$("#test3-div").css({opacity: 0.2}).wheel({
		color: 'red',
		width: 6,
		height: 8,
		spokes: 7,
		position: 'left'});

	$("#test4-div").css({opacity: 0.2}).wheel({
		color: 'green',
		width: 6,
		height: 8,
		spokes: 24,
		position: 'bottom'});

	$("canvas").click(function() {
		$(this).prev('div').wheel('destroy');			
	});

	$("div").click(function() {
		$(this).wheel('destroy');			
	});
}
