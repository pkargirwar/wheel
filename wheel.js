(function($) {
"use strict";
var interval = 60;
var increment;

$.fn.wheel = function(options) {
	//adjust canvas position according to current scroll position
	$(window).unbind('scroll').bind('scroll', function() {
		$(".wheel-canvas").each(function() {
			var pos = $(this).prev('div').offset();
			$(this).offset({top: pos.top , left: pos.left});
		});
	});
	if (options === "destroy") {
		return this.each(function() {
			destroy.apply(this);
		});
	}
	var args = arguments;	
	return this.each(function() {
		create.apply(this, args);
	});

};

function destroy()
{
	$(this).next('.wheel-canvas').remove();
	var wheel = $(this).data('wheel');
	if (!! wheel) wheel.stop();
}

function create()
{
	var settings = {
		background: 'rgba(255, 255, 255, 0)',
		color: '#222222',
		width: 2,
		height: 8,
		position: 'center',
		fallback: 'Loading',
		spokes: 12,
		increment: 30,
	};

	if (arguments[0] !== null) {
		settings = $.extend(settings, arguments[0]);
	}

	settings.increment = 360 / settings.spokes;
	var w = $(this).width();
	var h = $(this).height();
	var pos = $(this).offset();
	var cl;
	if (! window.CanvasRenderingContext2D) {
		//position loading dialog at the center of parent div
		$(this).after('<div class="wheel-canvas">' + settings.fallback +
				'</div>');
		cl = $(this).next('.wheel-canvas').css({
			position: 'fixed',
			background: 'yellow',
			color: 'black',
			'font-style': 'italic',
			width: 'auto',
			height: 'auto'
		});
		var wl = cl.width();
		var hl = cl.height();
		cl.offset({
			top: pos.top + (h - hl) / 2,
			left: pos.left + (w - wl) / 2
		});

		return;
	}
	//create and position canvas
	$(this).after('<canvas class="wheel-canvas"></canvas>');
	cl = $(this).next('.wheel-canvas');
	cl.css({
		position: 'fixed',
		top: pos.top,
		left: pos.left
	});

	var canvas = cl.get(0); 
	var context = canvas.getContext('2d');
	canvas.width = w;
	canvas.height = h;

	//dist is the min distance from center for each rectangle which prevents
	//rectangles overlapping.
	var tanThetaBy2 = Math.tan((360 / (2 * settings.spokes)) * Math.PI / 180);
	settings.dist = (settings.width / 2) / tanThetaBy2;
	//just increase dist slightly so that rectangles are clear of each other
	settings.dist += 4;

	var offX = w / 2;
	var offY = h / 2;
	var ht = settings.height;
	var dist = settings.dist;
	switch (settings.position) {
		case 'left':
		offX = ht + dist + 5; 
		offY = h / 2;			
		break;

		case 'right':
		offX = w - (ht + dist + 5); 
		offY = h / 2;			
		break;

		case 'top':
		offX = w / 2;
		offY = ht + dist + 5; 
		break;

		case 'bottom':
		offX = w / 2;
		offY = h - (ht + dist + 5); 
		break;
	}

	$(this).data('wheel', new wheel(context, settings, offX, offY));
}

function wheel(context, settings, offX, offY)
{
	var ctx = this;
	this.context = context;
	this.settings = settings;
	this.rot = 0;
	this.loop = function() {
		var context = ctx.context;
		var settings = ctx.settings;
		var height = settings.height;
		var dist = settings.dist;

		context.setTransform(1, 0, 0, 1, 0, 0);
		context.translate(offX, offY);
		context.clearRect(
			-(height + dist),
			-(height + dist),
			2 * (height + dist),
			2 * (height + dist));

		context.fillStyle = settings.background;
		context.fillRect(
			-(height + dist),
			-(height + dist),
			2 * (height + dist),
			2 * (height + dist));

		ctx.rot += settings.increment;
		var radians = ctx.rot * Math.PI / 180;
		context.rotate(radians);
		draw(context, settings, 1);
	}
	
	this.timer = setInterval(function() {ctx.loop();}, interval);
	this.stop = function() {
		clearInterval(ctx.timer);
	}
}

function draw(context, settings, gradient)
{
	//gradient = 0 => no gradient
	var angle = 0;
	var radians;

	for (var i = 0; i < settings.spokes; i++) {
		context.globalAlpha =
		(1 - ((settings.spokes - i) / settings.spokes) * gradient); 
		radians = angle * Math.PI / 180;
		context.rotate(radians);

		context.fillStyle = settings.color;
		context.fillRect(
			-0.5 * settings.width,
			-(settings.dist + settings.height),
			settings.width,
			settings.height);
		angle = settings.increment;
	}
}

})(jQuery);
